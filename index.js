#! /usr/bin/env node

var shell = require("shelljs");
var readlineSync = require('readline-sync');
var service = 'services';
var feature = 'features';
var language_path = 'i18n';
var source_path = '/src/';
var test_path = '__tests__';

shell.exec("echo Welcome to wei helper to generater the service files or feature files for new development");
 
var target = readlineSync.question('Now, what do you want to create? (services or features) ');

if (target !== service && target !== feature ){
    console.log('Wrong commands ' + target + '!');
    process.exit(1);
} 

var name = readlineSync.question('what is the name of the services or features you want to create? ');

console.log( target + ' ' + name );

var currentDIR = shell.pwd();
var path = currentDIR + source_path + target + '/' + name;


if (shell.test('-e', path)) { 
    console.log('Service/Feature name exists');
} else {

    // Go to the root folder
    shell.cd(currentDIR + source_path + target );

    // Create the feature or service folder
    shell.mkdir('-p', name);
    
    shell.cd(currentDIR + source_path + target + '/' + name);

    // Create file in folder
    if (target === service){
        // Create service files
        shell.touch(['index.js', 'api.js', 'actions.js', 'actionTypes.js', 'mock.js', 'reducer.js']);
    } else {
        // Create the feature files
        shell.touch(['index.js', name+'.js', 'style.js']);
        shell.mkdir('-p', language_path);
        shell.cd(currentDIR + source_path + target + '/' + name + '/' + language_path);
        shell.touch(['en.json', 'fr.json']);
        shell.cd(currentDIR + source_path + target + '/' + name);
    }

    // Create the test folder and file
    shell.mkdir('-p', test_path);
    shell.cd(currentDIR + source_path + target + '/' + name + '/' + test_path);
    shell.touch([name+'-test.js']);

    shell.exec("echo All files have been generated on " + path + ", happy coding! ");

}
process.exit(1);






 